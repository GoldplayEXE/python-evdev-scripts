# python-evdev-scripts

Some scripts using the python-evdev library to make a macro keyboard or access weird input devices.

WARNING: Don't try this scripts on your keyboard if you have only one connected

## Requirements

For this scripts to work you need to be using evdev as your Xorg input driver,
the "python-evdev" library ("python -m pip install python-evdev").
Furthermore the "keyboard_blank" and "macro_keyboard" scripts use the pattern matching feature, introduced in python3.10 (currently a pre-release), but could easily be rewritten for earlier versions.

## Usage

The scripts need to invoked with the /dev/input/ file pointing to the device whose inputs you want to catch.

For example:
./keys_test /dev/input/by-id/usb-Rapoo_Rapoo_Gaming_Keyboard-event-kbd


"keys_test" and "test" can be used to find the input type you want to act on

"keyboard_blank" is a template file for a macro keyboard

"macro_keyboard" is an example for a (sparsely populated) macro keyboard
